package scws;

public class Sebesseg implements Comparable<Sebesseg> {
	
	public final static String MERTEKEGYSEG = "m/s";
	
	private double ertek;
	
	public Sebesseg(double val, String me) throws InvalidUnitException {
        switch(me) 
        { 
            case "m/s": 
                ertek = val; 
                break;
            case "km/h": 
                ertek = val * 0.28; 
                break;
            case "mi/h": 
                ertek = val * 0.45; 
                break;
            case "knot": // 1 nm/h
                ertek = val * 0.51; 
                break;
            default:
            	throw new InvalidUnitException(); 
        } 
	}

	@Override
	public int compareTo(Sebesseg o) {		
		return Double.compare(this.ertek, o.getErtek());
	}

	public double getErtek() {
		return ertek;
	}

	public void setErtek(double ertek) {
		this.ertek = ertek;
	}
	
	public Sebesseg mul(Sebesseg masik) throws InvalidUnitException {
		return new Sebesseg( this.ertek * masik.getErtek(), "m/s");
	}
	
	public Sebesseg div(Sebesseg masik) throws InvalidUnitException {
		return new Sebesseg( this.ertek / masik.getErtek(), "m/s");
	}
}