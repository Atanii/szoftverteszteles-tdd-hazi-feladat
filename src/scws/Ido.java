package scws;

public class Ido implements Comparable<Ido> {
	
	public final static String MERTEKEGYSEG = "s";
	
	private int ertek;
	
	public Ido(int val, String me) throws InvalidUnitException {
        switch(me) 
        {
            case "s": 
                ertek = val; 
                break;
            case "m": 
                ertek = val * 60; 
                break;
            case "h": 
                ertek = val * 60 * 60; 
                break;
            default:
            	throw new InvalidUnitException(); 
        } 
	}

	@Override
	public int compareTo(Ido o) {		
		return Integer.compare(this.ertek, o.getErtek());
	}

	public int getErtek() {
		return ertek;
	}

	public void setErtek(int ertek) {
		this.ertek = ertek;
	}
}