package scws;

public class Hossz implements Comparable<Hossz> {
	
	public final static String MERTEKEGYSEG = "m";
	
	private double ertek;
	
	public Hossz(double val, String me) throws InvalidUnitException {
        switch(me) 
        { 
            case "cm": 
                ertek = val / 100.0; 
                break;
            case "m": 
                ertek = val; 
                break;
            case "km": 
                ertek = val * 1000.0; 
                break;
            case "in": 
                ertek = (val * 2.54) / 100.0; 
                break;
            case "ft": 
                ertek = ( (val * 12) * 2.54 ) / 100.0; 
                break;
            case "yd": 
                ertek = (((val * 3) * 12) * 2.54) / 100.0; 
                break;
            case "mile": 
                ertek = ((((val * 1760) * 3) * 12) * 2.54) / 100.0; 
                break;
            case "nm": 
                ertek = val * 1852; 
                break;
            default:
            	throw new InvalidUnitException(); 
        } 
	}

	@Override
	public int compareTo(Hossz o) {		
		return Double.compare(this.ertek, o.getErtek());
	}

	public double getErtek() {
		return ertek;
	}

	public void setErtek(double ertek) {
		this.ertek = ertek;
	}
}
