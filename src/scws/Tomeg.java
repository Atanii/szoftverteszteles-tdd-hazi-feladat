package scws;

public class Tomeg implements Comparable<Tomeg> {
	
	public final static String MERTEKEGYSEG = "kg";
	
	private double ertek;
	
	public Tomeg(double val, String me) throws InvalidUnitException {
        switch(me) 
        { 
            case "g": 
                ertek = val / 1000.0; 
                break;
            case "kg": 
                ertek = val; 
                break;
            case "t": 
                ertek = val * 1000.0; 
                break;
            case "oz": 
                ertek = (val * 28.34952) / 1000.0; 
                break;
            case "lb": 
                ertek = ( (val * 16) * 28.34952 ) / 1000.0; 
                break;
            default:
            	throw new InvalidUnitException(); 
        } 
	}

	@Override
	public int compareTo(Tomeg o) {		
		return Double.compare(this.ertek, o.getErtek());
	}

	public double getErtek() {
		return ertek;
	}

	public void setErtek(double ertek) {
		this.ertek = ertek;
	}
}
