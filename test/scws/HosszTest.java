package scws;

import static org.junit.Assert.*;

import org.junit.Test;

public class HosszTest {

	@Test
	public void testLetrehozas() throws InvalidUnitException {
		assertNotNull(new Hossz(1.0, "m"));
	}
	
	@Test
	public void testOsszeHasonLitasEgyenlo() throws InvalidUnitException {
		assertEquals( 0, (new Hossz(1.0, "m")).compareTo(new Hossz(1.0, "m")) );
	}
	
	@Test
	public void testOsszeHasonLitasMasodikNagyobb() throws InvalidUnitException {
		assertEquals( -1, (new Hossz(1.0, "m")).compareTo(new Hossz(10.0, "m")) );
	}
	
	@Test
	public void testOsszeHasonLitasElsoNagyobb() throws InvalidUnitException {
		assertEquals( 1, (new Hossz(10.0, "m")).compareTo(new Hossz(1.0, "m")) );
	}
	
	@Test(expected = InvalidUnitException.class)
	public void testLetrehozasNemLetezoMertekegyseggel() throws InvalidUnitException {
		new Hossz(1.0, "z");
	}
	
	@Test
	public void testOsszeHasonLitasEgyenloMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( 0, (new Hossz(1760.0, "yd")).compareTo(new Hossz(1.0, "mile")) );
	}
	
	@Test
	public void testOsszeHasonLitasMasodikNagyobbMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( -1, (new Hossz(1.0, "m")).compareTo(new Hossz(10000.0, "cm")) );
	}
	
	@Test
	public void testOsszeHasonLitasElsoNagyobbMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( 1, (new Hossz(5.0, "km")).compareTo(new Hossz(1.5, "nm")) );
	}
	
}
