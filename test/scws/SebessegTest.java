package scws;

import static org.junit.Assert.*;

import org.junit.Test;

public class SebessegTest {

	@Test
	public void testLetrehozas() throws InvalidUnitException {
		assertNotNull(new Sebesseg(1, "m/s"));
	}
	
	@Test
	public void testOsszeHasonLitasEgyenlo() throws InvalidUnitException {
		assertEquals( 0, (new Sebesseg(1, "m/s")).compareTo(new Sebesseg(1, "m/s")) );
	}
	
	@Test
	public void testOsszeHasonLitasMasodikNagyobb() throws InvalidUnitException {
		assertEquals( -1, (new Sebesseg(1, "km/h")).compareTo(new Sebesseg(10, "km/h")) );
	}
	
	@Test
	public void testOsszeHasonLitasElsoNagyobb() throws InvalidUnitException {
		assertEquals( 1, (new Sebesseg(10, "km/h")).compareTo(new Sebesseg(1, "km/h")) );
	}
	
	@Test(expected = InvalidUnitException.class)
	public void testLetrehozasNemLetezoMertekegyseggel() throws InvalidUnitException {
		new Sebesseg(1, "d");
	}
	
	@Test
	public void testOsszeHasonLitasEgyenloMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( 0, (new Sebesseg(0.28, "m/s")).compareTo(new Sebesseg(1, "km/h")) );
	}
	
	@Test
	public void testOsszeHasonLitasMasodikNagyobbMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( -1, (new Sebesseg(1, "m/s")).compareTo(new Sebesseg(10000, "km/h")) );
	}
	
	@Test
	public void testOsszeHasonLitasElsoNagyobbMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( 1, (new Sebesseg(100, "km/h")).compareTo(new Sebesseg(12, "m/s")) );
	}

}
