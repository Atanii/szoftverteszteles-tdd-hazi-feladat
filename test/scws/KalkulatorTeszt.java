package scws;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class KalkulatorTeszt {
	
	private static Kalkulator k;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Tomeg t = new Tomeg(10000.0, "t");
		Hossz h = new Hossz(120.0, "m");
		Sebesseg s = new Sebesseg(5.0, "m/s");
		Hajo sajat = new Hajo(t, h, s);
		k = new Kalkulator(sajat);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		k = null;
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws InvalidUnitException {
		Tomeg t = new Tomeg(8000.0, "t");
		Hossz h = new Hossz(100.0, "m");
		Sebesseg s = new Sebesseg(5.0, "m/s");
		Hajo masik = new Hajo(t, h, s);
		Pozicio p = new Pozicio(new Hossz(2, "nm"), new Hossz(2, "nm"));
		int irany = 270;		
		assertEquals("Lass�ts!", k.utkozik_e(masik, p, irany));		
	}

}
