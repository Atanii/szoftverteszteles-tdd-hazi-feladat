package scws;

import static org.junit.Assert.*;

import org.junit.Test;

public class TomegTest {

	@Test
	public void testLetrehozas() throws InvalidUnitException {
		assertNotNull(new Tomeg(1.0, "kg"));
	}
	
	@Test
	public void testOsszeHasonLitasEgyenlo() throws InvalidUnitException {
		assertEquals( 0, (new Tomeg(1.0, "kg")).compareTo(new Tomeg(1.0, "kg")) );
	}
	
	@Test
	public void testOsszeHasonLitasMasodikNagyobb() throws InvalidUnitException {
		assertEquals( -1, (new Tomeg(1.0, "kg")).compareTo(new Tomeg(10.0, "kg")) );
	}
	
	@Test
	public void testOsszeHasonLitasElsoNagyobb() throws InvalidUnitException {
		assertEquals( 1, (new Tomeg(10.0, "kg")).compareTo(new Tomeg(1.0, "kg")) );
	}
	
	@Test(expected = InvalidUnitException.class)
	public void testLetrehozasNemLetezoMertekegyseggel() throws InvalidUnitException {
		new Tomeg(1.0, "d");
	}
	
	@Test
	public void testOsszeHasonLitasEgyenloMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( 0, (new Tomeg(1000.0, "g")).compareTo(new Tomeg(1.0, "kg")) );
	}
	
	@Test
	public void testOsszeHasonLitasMasodikNagyobbMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( -1, (new Tomeg(1.0, "kg")).compareTo(new Tomeg(10000.0, "t")) );
	}
	
	@Test
	public void testOsszeHasonLitasElsoNagyobbMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( 1, (new Tomeg(10.1, "lb")).compareTo(new Tomeg(0.5, "oz")) );
	}

}
