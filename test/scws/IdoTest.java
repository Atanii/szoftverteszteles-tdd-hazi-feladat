package scws;

import static org.junit.Assert.*;

import org.junit.Test;

public class IdoTest {

	@Test
	public void testLetrehozas() throws InvalidUnitException {
		assertNotNull(new Ido(1, "s"));
	}
	
	@Test
	public void testOsszeHasonLitasEgyenlo() throws InvalidUnitException {
		assertEquals( 0, (new Ido(1, "s")).compareTo(new Ido(1, "s")) );
	}
	
	@Test
	public void testOsszeHasonLitasMasodikNagyobb() throws InvalidUnitException {
		assertEquals( -1, (new Ido(1, "h")).compareTo(new Ido(10, "h")) );
	}
	
	@Test
	public void testOsszeHasonLitasElsoNagyobb() throws InvalidUnitException {
		assertEquals( 1, (new Ido(10, "h")).compareTo(new Ido(1, "h")) );
	}
	
	@Test(expected = InvalidUnitException.class)
	public void testLetrehozasNemLetezoMertekegyseggel() throws InvalidUnitException {
		new Ido(1, "d");
	}
	
	@Test
	public void testOsszeHasonLitasEgyenloMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( 0, (new Ido(60, "s")).compareTo(new Ido(1, "m")) );
	}
	
	@Test
	public void testOsszeHasonLitasMasodikNagyobbMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( -1, (new Ido(1, "m")).compareTo(new Ido(10000, "h")) );
	}
	
	@Test
	public void testOsszeHasonLitasElsoNagyobbMasMertekegyseggel() throws InvalidUnitException {
		assertEquals( 1, (new Ido(10, "h")).compareTo(new Ido(342, "s")) );
	}

}
